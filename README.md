Perlin Noise
============

History
-------
I started this project as an experimentation with my friend bog. The result was that impressive that I decided to use it in another project I started called Ultimate-Magic.

Unfortunately, I did not have the skill required to create this RPG as I wanted.

Now, I am working on it in order to make it quick and to be able to reuse it in any project. 

Issues
------
* Make the Perlin-class easy to use
* Make it editable 
* Allow it to have some pre-registered configuration rather than a full definided use
* Make it fast and modern (with C++11)

Controls
--------
To test this project, I made a main with SFML library to draw one interpretation of this Perlin noise.
Here are the controls :
* Press CTRL to control the limits of each components (sea, sand, grass and ground)
* Press the firt letter of the color you want to modify 
    * 'B' for "Blue" to modify sea max limit
    * 'Y' for "Yellow" to modify sand max limit
    * 'G' for "Green" to modify grass max limit and deduce ground min limit
* Press R to create a whole new noise

Thanks for reading, here is a duck:
https://fr.wikipedia.org/wiki/Canard

VanDog