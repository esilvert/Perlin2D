#include <Perlin.hpp>

#include <cstdlib>
#include <cmath>
#include <cassert>
#include <random>
#include <chrono>

Perlin::Perlin(size_t w, size_t h, size_t step, bool autoRandom) :
  m_step(step)
  , m_width(w)
  , m_height(h)
{
 
  initTab();
  if( autoRandom )
    {
      randomize();
    }
 
  interpolate();

}

void Perlin::reset(void)
{
  initTab();
  randomize();
  interpolate();
}

void Perlin::initTab()
{
  for( size_t i(0) ; i < m_height ; ++i )
    {
      std::vector<double> temp;
      for( size_t j(0) ; j < m_height ; ++ j )
	{
	  temp.push_back(0.);
	  // m_tab.emplace_back( std::vector<double>(m_width, 0) );
	}
      m_tab.push_back(temp);
    }
}

void Perlin::randomize()
{
  std::default_random_engine generator(std::chrono::steady_clock::now().time_since_epoch().count());
  std::uniform_real_distribution<double> distrib(0,100);
  //std::normal_distribution<double> distrib(0,100);
  //std::lognormal_distribution<double> distrib(0,2);
  //std::chi_squared_distribution<double> distrib(40);
  //std::fisher_f_distribution<double> distrib(1.0,1.0);
  //std::discrete_distribution<int> distrib{0,10,20,30,40,50,60,70,80,90,100,100,100,100};



  // create the rect values
  for( size_t i(0) ; i < m_height ; i++ )
    {
      std::vector<double> temp;
      for( size_t j(0) ; j < m_width ; j++ )
	{
	  if( (i % m_step == 0 || i == m_height - 1)
	      && (j % m_step == 0 || j == m_width - 1)
	      )
	    {
	      temp.push_back( distrib(generator) );
	    }
	  else
	    {
	      temp.push_back(0);
	    }
	}
      m_tab[i].swap(temp);
    }
}

double Perlin::linear_interpolation(double a, double b, double t) const
{
  return a * (1-t) + b * t;
}

double Perlin::polynomial_interpolation(double a, double b, double t) const
{
  t *= 3.141592653589; // PI
  double u = (1 - cos(t)) * .5;
  return linear_interpolation(a,b,u);

}


bool Perlin::inMap(size_t i, size_t j) const
{
  return( i < m_height && j < m_width ); // > 0 is useless when using size_t
}

void Perlin::interpolate()
{
  size_t Ax,Ay,Bx,By,Cx,Cy,Dx,Dy,Ex,Ey,Fx,Fy;

  for( size_t i(0) ; i < m_width ; i++ )
    {
      for( size_t j(0) ; j < m_height ; j++ )
	{
	  if( i % m_step == 0 && i < m_width )
	    {
	      Ay = i;
	    }
	  if( j % m_step == 0 && j < m_height )
	    {
	      Ax = j;
	    }

	  /* using std::min in order to interpolate to the end */
	 
	  By = Ay;
	  Bx = std::min(Ax + m_step, m_width -1);
		 
	  Cx = std::min(Ax + m_step, m_width - 1);
	  Cy = std::min(Ay + m_step, m_height - 1);

	  Dx = Ax;
	  Dy = std::min(Ay + m_step, m_width - 1);

	  Ex = j;
	  Ey = Ay;

	  Fx = j;
	  Fy = Dy;

	  // Perlin
	  if( inMap(Cx,Cy) )
	    {
	      m_tab[Ex][Ey] = polynomial_interpolation(m_tab[Ax][Ay],m_tab[Bx][By],(double)(Ex-Ax)/(double)(Bx-Ax));
	      m_tab[Fx][Fy] = polynomial_interpolation(m_tab[Dx][Dy],m_tab[Cx][Cy],(double)(Fx-Dx)/(double)(Cx-Dx));
	      m_tab[j][i] = polynomial_interpolation(m_tab[Ex][Ey],m_tab[Fx][Fy],(double)(i-Ey)/(double)(Fy-Ey));
	    }
	}
    }
}

size_t Perlin::random(size_t min, size_t max) const
{
  return rand() % max + min;
}

void Perlin::display()
{
  for( size_t i(0) ; i < m_height ; i++ )
    {
      for( size_t j(0) ; j < m_width ; j++ )
	{
	  std::cout << m_tab[i][j] << " ";
	}
      std::cout << '\n';
    }
}

void Perlin::setTab( std::vector<std::vector<double> >& tab)
{
  assert( tab.size() != 0);
  m_tab = tab;
  m_width = tab[0].size();
  m_height = tab.size();
  interpolate();
}



/* =============== GETTER =============== */
const double Perlin::get(size_t i, size_t j) const
{
  assert( i < m_height && j < m_width);
  return m_tab[i][j];
}

const size_t Perlin::getWidth() const
{
  return m_width;
}

const size_t Perlin::getHeight() const
{
  return m_height;
}

Perlin::~Perlin()
{
  //dtor
}
