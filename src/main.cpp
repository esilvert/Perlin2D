#include <iostream>

#include <cstdlib>
#include <ctime>
#include <Perlin.hpp>
#include <SFML/Graphics.hpp>
#include <memory>
#include <chrono>
#include <algorithm>
#include <cassert>

#define WINDOW_HEIGHT 900
#define WINDOW_WIDTH 1640
#define N_TILES 1000
#define TILE_WIDTH WINDOW_WIDTH / N_TILES
#define TILE_HEIGHT WINDOW_HEIGHT / N_TILES
#define STEP 15


int main()
{
  srand(time(NULL));

  size_t sea_limit = 30;
  size_t sand_limit = 40;
  size_t ground_limit = 100;
    
  std::unique_ptr<Perlin> p( new Perlin(N_TILES, N_TILES,STEP) );
  sf::RectangleShape rc( sf::Vector2f(TILE_WIDTH, TILE_HEIGHT) );
  sf::RenderWindow window(  sf::VideoMode( WINDOW_WIDTH, WINDOW_HEIGHT ),"Perlin_2D");

  sf::VertexArray va(sf::Quads);
  bool reset = true;
  bool limitChanged = true;
  while( window.isOpen() )
    {
      std::chrono::time_point<std::chrono::steady_clock> start  = std::chrono::steady_clock::now();
      sf::Event event;
      while( window.pollEvent(event) )
        {
	  switch(event.type)
            {
            case sf::Event::Closed : window.close(); break;
            case sf::Event::KeyPressed:
	      {
                switch(event.key.code)
		  {
		  default:break;
		  case sf::Keyboard::Escape: window.close();break;

		  case sf::Keyboard::R:
		    {
		      p->reset();
		      reset = true;
		    }break;
		  }
	      }break;
            default:break;
            }
        }

      // CONTROL
      // SEA UP
      if(
	 sf::Keyboard::isKeyPressed(sf::Keyboard::LControl)
	 && sf::Keyboard::isKeyPressed(sf::Keyboard::B)
	 && sf::Keyboard::isKeyPressed(sf::Keyboard::Add)
	 )
	{sea_limit = (sea_limit <= 98) ? sea_limit + 2: sea_limit; limitChanged = true;}

      // SEA DOWN
      if(
	 sf::Keyboard::isKeyPressed(sf::Keyboard::LControl)
	 && sf::Keyboard::isKeyPressed(sf::Keyboard::B)
	 && sf::Keyboard::isKeyPressed(sf::Keyboard::Subtract)
	 )
	{sea_limit = (sea_limit >= 2) ? sea_limit - 2: sea_limit; limitChanged = true;}

      // SAND UP
      if(
	 sf::Keyboard::isKeyPressed(sf::Keyboard::LControl)
	 && sf::Keyboard::isKeyPressed(sf::Keyboard::Y)
	 && sf::Keyboard::isKeyPressed(sf::Keyboard::Add)
	 )
	{sand_limit = (sand_limit <= 98) ? sand_limit + 2: sand_limit; limitChanged = true;}

      // SAND DOWN
      if(
	 sf::Keyboard::isKeyPressed(sf::Keyboard::LControl)
	 && sf::Keyboard::isKeyPressed(sf::Keyboard::Y)
	 && sf::Keyboard::isKeyPressed(sf::Keyboard::Subtract)
	 )
	{sand_limit = (sand_limit >= 2) ? sand_limit - 2: sand_limit; limitChanged = true;}

      // LAND UP
      if(
	 sf::Keyboard::isKeyPressed(sf::Keyboard::LControl)
	 && sf::Keyboard::isKeyPressed(sf::Keyboard::G)
	 && sf::Keyboard::isKeyPressed(sf::Keyboard::Add)
	 )
	{ground_limit = (ground_limit <= 98) ? ground_limit + 2: ground_limit; limitChanged = true;}

      // LAND DOWN
      if(
	 sf::Keyboard::isKeyPressed(sf::Keyboard::LControl)
	 && sf::Keyboard::isKeyPressed(sf::Keyboard::G)
	 && sf::Keyboard::isKeyPressed(sf::Keyboard::Subtract)
	 )
	{ground_limit = (ground_limit >= 2) ? ground_limit - 2: ground_limit; limitChanged = true;}
	
      window.clear( sf::Color(4,139,154));

      size_t width = p->getWidth();
      size_t height= p->getHeight();

      // first loop => fill it
      if ( va.getVertexCount() == 0 )
	{
	  for ( size_t k(0) ; k < width * height * 4 ; ++k)
	    {
	      sf::Vertex v;
	      va.append(v);
	    }
	}

      if( limitChanged || reset )
	{
	  for( size_t i(0) ; i < height ; i++)
	    {
	      for( size_t j(0) ; j < width ; j++ )
		{     
		  double value = p->get(j, i);
	      
		  /// COLORS
		  sf::Color color;
		  int red = (value > sea_limit && value < sand_limit ) ? value  / sand_limit * 255 : 0;
		  int green = (value > sea_limit && value < sand_limit ) ? value / sand_limit * 255 : ((value > sand_limit && value < ground_limit ) ? value / ground_limit * 255 : 0);
		  int blue = (value < sea_limit) ?  (value ) / (sea_limit ) * 205 + 50 : 0;
		  // MAX COLOR
		  if( value > ground_limit )
		    {
		      int greyLevel = 200 *value/100;
		      red = greyLevel;
		      blue = greyLevel;
		      green = std::min(255, (int)(greyLevel*2));
		    }
		  color = sf::Color( red, green, blue);


		  size_t vaIndex = j * 4  + i * width * 4;
		  size_t max = va.getVertexCount() - 1;
		  // update color
		  for ( size_t k(vaIndex) ; k < max && k < vaIndex + 4 ; ++k )
		    {
		      assert(k >= 0 && k < va.getVertexCount());
		      va[k].color = color;
		    }

		  // update position
		
		  va[vaIndex].position = sf::Vector2f(i * TILE_WIDTH,       j * TILE_HEIGHT);
		  va[vaIndex + 1].position = sf::Vector2f((i + 1) * TILE_WIDTH, j * TILE_HEIGHT);
		  va[vaIndex + 2].position = sf::Vector2f((i + 1) * TILE_WIDTH, (j + 1) * TILE_HEIGHT);
		  va[vaIndex + 3].position = sf::Vector2f(i * TILE_WIDTH,       (j + 1) * TILE_HEIGHT);

		}
	    }
	}
      reset = false;
      limitChanged = false;
      
      window.draw(va);
      window.display();

      // FPS
      std::chrono::time_point<std::chrono::steady_clock> end = std::chrono::steady_clock::now();
      std::chrono::duration<double> dt = end - start;
      window.setTitle("Perlin2D - FPS : " + std::to_string(1.0f/dt.count()));
    }      
  return 0;
}
